# Scripts

- `npm start` (starts development server)
- `npm test` (runs some tests)
- `npm run build` (optionally analyze source maps with `npm run analyze`)
- `npm deploy` (deploys to http://vbrh-immalle.github.io/ReactAlphabetTrainer)
"homepage": "http://vbrh-immalle.github.io/ReactAlphabetTrainer",

# TODO

- animated progress bar?
- `FocusableEnableableTextInput` testing: wat kan er allemaal met testing en hoe werken effects ook weeral in deze component?
