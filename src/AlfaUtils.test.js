import {
    getRandomInt,
    getRandomIntInclusive,
    alphabet,
    questions,
    getRandomQuestionLetter,
    getCorrectPreLetter,
    getCorrectPostLetter,
 } from './AlfaUtils';

it('should return a random int from 1 to 10 (exclusive)', () => {

    for(var i=0; i<100; i++) {
        const x = getRandomInt(1, 10);
        //console.log(x);
        expect(x).toBeGreaterThanOrEqual(1);
        expect(x).toBeLessThan(10);
    }
    
});

it('should return a random int from 1 to 10 (inclusive)', () => {

    for(var i=0; i<100; i++) {
        const x = getRandomIntInclusive(1, 10);
        //console.log(x);
        expect(x).toBeGreaterThanOrEqual(1);
        expect(x).toBeLessThanOrEqual(10);
    }
    
});

it('should return a random letter but not A or Z', () => {

    for(var i=0; i<100; i++) {
        const x = getRandomQuestionLetter();
        //console.log(x);
    }

});

it('should give the previous letter in alphabet', () => {

    expect(getCorrectPreLetter('B')).toBe('A');
    expect(getCorrectPreLetter('Z')).toBe('Y');

});

it('should give the next letter in alphabet', () => {

    expect(getCorrectPostLetter('B')).toBe('C');
    expect(getCorrectPostLetter('Y')).toBe('Z');

});
