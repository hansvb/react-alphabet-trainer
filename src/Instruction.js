import React from 'react';
/** @jsx jsx */import { css, jsx } from "@emotion/core";

const InstructionsCss = css`

`;

export default (props) => {    
    return <div css={InstructionsCss}>
        {props.instructionText}
    </div>
}
