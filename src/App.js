import React, { useEffect, useReducer } from 'react';
import Question from './Question';
import Instruction from './Instruction';
import FocusableEnableableTextInput from './FocusableEnableableTextInput';
/** @jsx jsx */import { css, jsx } from "@emotion/core";
import Progress from './Progress';
import { 
    getRandomQuestionLetter,
    getCorrectPreLetter,
    getCorrectPostLetter,
} from './AlfaUtils';

const appStyle = css`
  text-align: center;  
`;

const appHeaderCss = css`
  font-family: 'Times new Roman';
  background-color: #282c34;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  font-size: calc(28px + 2vmin);
  color: white;
  padding-top: 10px;
`;

const playfieldStyle = css`
  display: flex;
  flex-direction: row;
`;

const progressStyle = css`

`;



const initialStateInit = () => {
    const questionLetter = getRandomQuestionLetter();
    const instructionText = `Geef de letter die voor ${questionLetter} komt.`;

    return {
        gameState: 'ENTERING_FIRST_LETTER',
        questionLetter,
        instructionText,
        preLetter: '',
        postLetter: '',
        preLetterFocus: true,
        postLetterFocus: false,
        preLetterEnabled: true,
        postLetterEnabled: false,
    }
}

function reducer(state, action) {
    console.log('in reducer, state: ')
    console.log(state);
    console.log('in reducer, action: ');
    console.log(action);
    switch (action.type) {
        case 'ENTERED_FIRST_LETTER_ACTION':
            const preLetter = action.preLetter.toUpperCase();
            const correctPreLetter = getCorrectPreLetter(state.questionLetter);

            if(preLetter === correctPreLetter) {
                return {
                    ...state,
                    gameState: 'ENTERING_SECOND_LETTER',
                    instructionText: `Juist! Geef nu de letter die na ${state.questionLetter} komt.`,
                    preLetter,
                    preLetterFocus: false,
                    preLetterEnabled: false,
                    postLetterFocus: true,
                    postLetterEnabled: true,
                }   
            } else {
                return {
                    ...state,
                    gameState: 'WRONG_AFTER_FIRST_LETTER',
                    instructionText: `Fout! Niet ${preLetter} maar ${correctPreLetter} komt voor ${state.questionLetter}!`,
                    preLetter,
                    preLetterFocus: false,
                    postLetterFocus: true,
                    preLetterEnabled: false,
                    postLetterEnabled: false,
                }
            }
        case 'ENTERED_SECOND_LETTER_ACTION':
            const postLetter = action.postLetter.toUpperCase();
            const correctPreLetter2 = getCorrectPreLetter(state.questionLetter);
            const correctPostLetter = getCorrectPostLetter(state.questionLetter);
            if(postLetter === correctPostLetter) {
                return {
                    ...state,
                    gameState: 'CORRECT',
                    instructionText: `${correctPreLetter2} ${state.questionLetter} ${correctPostLetter} is inderdaad juist!`,
                    postLetter,
                    postLetterFocus: false,
                    postLetterEnabled: false,
                }
            } else {
                return {
                    ...state,
                    gameState: 'WRONG_AFTER_SECOND_LETTER',
                    instructionText: `Fout! Niet ${postLetter} maar ${correctPostLetter} komt na ${state.questionLetter}!`,
                    postLetter,
                    postLetterFocus: false,
                    postLetterEnabled: false,
                }
            }
        case 'RESTART_ACTION':
            return initialStateInit();
        default:
            console.log("Unknown gameState in reducer! This shouldn't happen!");
    }
    console.log('before return reducer');
    console.log(state);
    return state;
}

const App = () => {
    const [state, dispatch] = useReducer(reducer, null, initialStateInit);
    console.log('in app');
    console.log(state);
    const preLetterHandler = l => dispatch({ type: 'ENTERED_FIRST_LETTER_ACTION', preLetter: l });
    const postLetterHandler = l => dispatch( {type: 'ENTERED_SECOND_LETTER_ACTION', postLetter: l })

    useEffect(() => {
        console.log('in use effect');
        console.log(state);
        if(state.gameState === 'CORRECT' 
        || state.gameState === 'WRONG_AFTER_FIRST_LETTER'
        || state.gameState === 'WRONG_AFTER_SECOND_LETTER') {
            console.log('restarting in 1.5 seconds...');
            setTimeout(() => {
                dispatch( {type: 'RESTART_ACTION'} );
            }, 1500);
        }
    });
    
    return <div css={appStyle}>
    <header css={appHeaderCss}>
      {/* <h1>Alfabet Trainer</h1> */}
      <div css={playfieldStyle}>
        <FocusableEnableableTextInput 
            onChange={preLetterHandler}
            value={state.preLetter}
            enabled={state.preLetterEnabled}
            giveFocus={state.preLetterFocus}
        />
        <Question letter={state.questionLetter} />
        <FocusableEnableableTextInput
            onChange={postLetterHandler}
            value={state.postLetter}
            enabled={state.postLetterEnabled}
            giveFocus={state.postLetterFocus}
        />
      </div>
      <Instruction instructionText={state.instructionText} />
      {/* <Progress ref={refProgress} /> */}
      {/* <Progress ref={refProgress} value={progressState.value} time={progressState.time} /> */}
    </header>
  </div>
}

export default App;
