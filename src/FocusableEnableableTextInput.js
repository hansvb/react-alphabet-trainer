import React from 'react';
/** @jsx jsx */import { css, jsx } from "@emotion/core";

const inputFormStyle = css`
    font-family: 'Luckiest Guy', cursive;
    font-size: 36px;
    background-color: #333;
    padding: 20px;
    width: 80px;
    color: #aaa;
    text-align: center;
    justify-content: center;
`;

const InnerTextInput = React.forwardRef((props, ref) => {
    
    const changeHandler = e => props.onChange(e.target.value.toUpperCase());
    
    return <input 
        css={inputFormStyle}
        ref={ref}
        type='text' 
        value={props.value}
        onChange={changeHandler}
        disabled={!props.enabled}
    />
});

const FocusableEnableableTextInput = props => {
    const ref = React.useRef(null);

    React.useEffect(() => {
        if(ref.current) {
            if(props.giveFocus) {
                ref.current.focus();
            } else {
                ref.current.blur();
            }
        }
    })

    return <InnerTextInput
        ref={ref}
        value={props.value}
        onChange={props.onChange}
        enabled={props.enabled}
    />
}

export default FocusableEnableableTextInput;