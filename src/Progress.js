import React from 'react';
/** @jsx jsx */import { css, jsx } from "@emotion/core";

const wrapperStyle = css`
    width: 600px;
`;

const outerStyle = css`
    width: 100%;
    background-color: #eee;
    //padding: 3px;
    //border-radius: 3px;
    //box-shadow: inset 0 1px 3px rgba(0, 0, 0, .2)
`;



const Progress = React.forwardRef((props, ref) => {
    //let innerStyleMod = innerStyle;
    // innerStyleMod += css`
    //     width: ${props.value}
    // `;

    const innerStyle = css`
        display: block;
        height: 8px;
        background-color: #333;
        //border-radius: 3px;
        width: ${props.value}%;
        transition: width ${props.time}ms ease-in-out;
    `;

    return <div css={wrapperStyle}>
        <div css={outerStyle}>
            <span ref={ref} css={innerStyle} onAnimationEnd={() => console.log('animend')}></span>
        </div>
    </div>
});

export default Progress