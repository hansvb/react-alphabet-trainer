import React from 'react';
/** @jsx jsx */import { css, jsx } from "@emotion/core";

const QuestionCss = css`
    justify-content: center;
    font-family: 'Luckiest Guy', cursive;
    font-size: 36px;
    background-color: gray;
    padding: 20px;
    width: 80px;
`;

const Question = (props) => (
    <div css={QuestionCss}>
        {props.letter}
    </div>
)

export default Question;
