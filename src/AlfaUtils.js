
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

const alphabetHelper = Array(26).fill('A');
const alphabet = alphabetHelper.map((c,i) => String.fromCharCode(c.charCodeAt(0) + i));
const questions = alphabet.slice(1, alphabet.length-1);

function getRandomQuestionLetter() {
    return questions[getRandomIntInclusive(0, questions.length - 1)];
}

function getCorrectPreLetter(questionLetter) {
    return String.fromCharCode(questionLetter.charCodeAt(0) - 1);
}

function getCorrectPostLetter(questionLetter) {
    return String.fromCharCode(questionLetter.charCodeAt(0) + 1);
}

export { 
    getRandomInt,
    getRandomIntInclusive,
    alphabet,
    questions,
    getRandomQuestionLetter,
    getCorrectPreLetter,
    getCorrectPostLetter,
}